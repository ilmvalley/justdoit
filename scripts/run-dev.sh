#!/usr/bin/bash

if [ -e "find_java.sh" ] ; then
    cd ..
fi

JAVACMD=$(bash scripts/find_java.sh)
JAVACMD+="java"

"$JAVACMD" -cp bin/ justDoIt.Main
