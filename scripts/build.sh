#!/usr/bin/bash

# go to project base if in scrips folder
if [ -e "find_java.sh" ] ; then
    cd ..
fi

# find javac
JAVACCMD=$(bash scripts/find_java.sh)
JAVACCMD+="javac"

# compile sources
cd src
find -name "*.java" | xargs "$JAVACCMD" -d ../bin/

# copy resources
cp -r interpreter/aas/patterns ../bin/interpreter/aas/patterns
cp justDoIt/test.txt ../bin/justDoIt/test.txt
cp interpreter/filter/meaningless_words ../bin/interpreter/filter/meaningless_words

echo "build terminated"
