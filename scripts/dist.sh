#!/usr/bin/bash

if [ -e "find_java.sh" ] ; then
    cd ..
fi

JARCMD=$(bash scripts/find_java.sh)
JARCMD+="jar"

if ! [ -e "build/" ] ; then
    mkdir build
fi

cd bin

"$JARCMD" -c -f ../build/JustDoIt.jar --main-class=justDoIt.Main .
echo "jar assembled"
