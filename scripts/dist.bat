@if "%DEBUG%" == "" @echo off

cd ..

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

@rem Find jar.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAR_EXE=jar.exe
%JAR_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto execute

echo.
echo ERROR: JAVA_HOME is not set and no 'jar' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAR_EXE=%JAVA_HOME%/bin/jar.exe

if exist "%JAR_EXE%" goto execute

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:execute
if not exist "build/" mkdir build
@rem run
"%JAR_EXE%" -c -f ../build/JustDoIt.jar --main-class=justDoIt.Main .

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
exit 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
