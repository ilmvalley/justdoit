package ui;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import interpreter.MessageProcessor;
import justDoIt.Message;

/**
 * a label containing the message
 */
public class MessageBox extends JLabel {
	
	private static final long serialVersionUID = -1448214504411419266L;
	
	/**
	 * whether it's the users message
	 */
	public boolean own;
	
	/**
	 * create the message box
	 * @param msg
	 *   the message base
	 */
	public MessageBox(Message msg) {
		super(msg.text);
		create(msg.own);
	}

	/**
	 * set all the visual properties
	 * @param own
	 *   whether it's the users message
	 */
	private void create(boolean own) {
		this.own = own;
		//create a border
		setBorder(new EmptyBorder(0,0,1,1)); //TODO: create a proper border
		if (own) {
			//add a background
			setBackground(Color.green); //TODO: Background is not shown
			//set alignment
			setHorizontalAlignment(RIGHT);
		} else {
			//add a background
			setBackground(Color.white);
			//set alignment
			setHorizontalAlignment(LEFT);
		}
		//get text
		String processedText = getText();
		//check for command
		if (processedText.startsWith("::")) {
			//commands are gray
			setForeground(Color.gray);
			//replace command by it's description
			processedText = MessageProcessor.getDescription(processedText);
		}
		
		//let's process the text
		if (!(processedText.startsWith("<html>") && processedText.endsWith("</html>"))) {
			//embed the message in html
			processedText = "<html>" + processedText + "</html>";
			//make line-breaks available in a label
			processedText = processedText.replace("\n", "<br>");
		}
		setText(processedText);
	}
}
