package ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import justDoIt.Log;
import justDoIt.Main;
import justDoIt.Message;
import interpreter.MessageProcessor;

/**
 * the chat window
 */
public class Chat extends JFrame {
	
	private static final long serialVersionUID = -6892727620468367015L;
	
	/**
	 * the visual container for the messages
	 */
	private final JPanel msgCont = new JPanel();
	
	/**
	 * a scroll pane for scrolling through the messages
	 */
	private final JScrollPane contScroller = new JScrollPane(msgCont);
	
	/**
	 * the text field where the user can write his messages
	 */
	private final JTextField writeField = new JTextField();
	
	/**
	 * the button to send messages
	 */
	private final JButton buttonWrite = new JButton("write");

	private ActionListener writeMessage = e -> {
		//log the message send action
		Log.verbose(e.toString());
		//get the message
		String msg = e.getActionCommand();
		//don't write empty messages
		if (msg.isEmpty())
			return;
		//add the message to the chat
		Main.addMessage(msg, true, false);
		//process the message
		MessageProcessor.msg(msg);
		//empty the write field
		writeField.setText("");
	};
	
	/**
	 * build the chat window
	 */
	public Chat() {
		//build a frame and give it a title
		super("Just Do It!");
		//build a layout for the contents
		GridBagLayout gridbag = new GridBagLayout();
		//properties for the contents
		GridBagConstraints gbc = new GridBagConstraints();
		//set the new layout
		setLayout(gridbag);
		//set the layout for the messages
		msgCont.setLayout(new GridLayout(0,1));
		
		//send user entry
		Main.addMessage("::open", true, true);
		
		//increase the size of the small components to resize h and v
		gbc.fill = GridBagConstraints.BOTH;
		//take up space with priority of 1
		gbc.weightx = 1;
		//take only the height needed
		gbc.weighty = 0;
		//use all the width available
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		//set properties for the label
		gridbag.setConstraints(WritingCaption.globalPanel, gbc);
		//add it to the frame
		add(WritingCaption.globalPanel);
		
		//add the messages to the message container
		for (Message message : Main.messages) {
			msgCont.add(new MessageBox(message));
		}
		//take up space with priority of 1
		gbc.weighty = 1;
		gbc.weightx = 1;
		//use all the width available
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		//set properties for the scrolling pane
		gridbag.setConstraints(contScroller, gbc);
		//add it to the frame
		add(contScroller);
		
		//add the writing listener to the text field
		writeField.addActionListener(writeMessage);
		//take only the height needed
		gbc.weighty = 0;
		//use width relative to other components
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		//set the properties for the text field
		gridbag.setConstraints(writeField, gbc);
		//add it to the frame
		add(writeField);
		
		//add an action listener for writing to the button
		buttonWrite.addActionListener(arg0 -> writeMessage.actionPerformed(new ActionEvent(buttonWrite, ActionEvent.ACTION_PERFORMED, writeField.getText(),System.currentTimeMillis(),0)));
		//take only the width needed
		gbc.weightx = 0;
		//use up the width left
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		//set the properties for the button
		gridbag.setConstraints(buttonWrite, gbc);
		//add it to the frame
		add(buttonWrite);
		//pack the frame and its contents
		pack();
		//set window maximized
		setExtendedState(MAXIMIZED_BOTH);
		//destroy if closed
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		//send close it if closed
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				Main.addMessage("::close",true,true);
			}
		});
		//show to the user
		setVisible(true);
	}

	/**
	 * add a message to the message container and update the displayed frame
	 * @param msg
	 *   the message to be added
	 */
	public void addMessage(Message msg) {
		//actually add the message
		msgCont.add(new MessageBox(msg));
		//show new messages
		msgCont.validate();
		//scroll down as far as possible
		//TODO: scroll down smoothly with a separate thread
		contScroller.getVerticalScrollBar().setValue(Integer.MAX_VALUE);
	}
}
