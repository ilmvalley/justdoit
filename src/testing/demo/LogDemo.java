package testing.demo;

import java.io.File;
import java.io.FileNotFoundException;

import justDoIt.Log;
import justDoIt.Main;

public class LogDemo {

	/**
	 * a demo of the Log class
	 * @param args
	 *   c for colorisation
	 */
	public static void main(String[] args) {
		if (Main.hasOption(args, "c", "colorize"))
			Log.colorizeOutput = true;
		
		try {
			Log.setLogFile(new File("log.txt"));
		} catch (FileNotFoundException e) {
			Log.error("Error while trying to set logfile",e);
		}
		Log.setLevel(Log.ALL); // full logging
		/*Log.setThreadLoggingLength(0); //no thread names shown in log
		Log.debug("a debug message without thread name");
		Log.setThreadLoggingLength(10);*/
		Log.verbose("a detailed debug information");
		Log.info("an information");
		Log.debug("a debug information");
		Log.action("an action");
		Log.message("an always shown message");
		Log.warning("a warning (unusual behavior)");
		Log.error("an error", new RuntimeException("a runtime exception"));
		Log.setLevel(Log.ERROR); //only error messages
		Log.warning("This message should have been ignored!");
	}

}
