package testing;

import java.util.List;

import interpreter.aas.AASTest;
import justDoIt.ConsoleStyle;
import justDoIt.Log;
import justDoIt.Main;
import testing.tests.CheckerTests;
import testing.tests.ObjectStoreTests;
import testing.tests.ResourceTests;
import testing.tests.SmokeTests;

public final class TestRunner {
	
	private static final List<TestCase> cases = List.of(
			new SmokeTests(),
			new ObjectStoreTests(),
			new CheckerTests(),
			new ResourceTests(),
			new AASTest()
		);
	
	public static class TestResults {
		private static int failed = 0;
		private static int passed = 0;
		
		public static void addSuccess() {
			passed++;
		}

		public static void addFail() {
			failed++;
		}

		public static boolean noFails() {
			return failed == 0;
		}

		public static int getPassed() {
			return passed;
		}
		
		public static int getFailed() {
			return failed;
		}
	}
	
	public static void results() {
		Log.outPrintln(Log.stamp("testing result") + "all tests terminated");
		Log.colorizeOut(ConsoleStyle.Foreground.GREEN);
		Log.outPrintln("tests passed: " + TestRunner.TestResults.getPassed());
		Log.colorizeOut(ConsoleStyle.Reset.RESET_ALL);
		Log.flush();
		Log.colorizeErr(ConsoleStyle.Foreground.RED);
		Log.errPrintln("tests failed: " + TestRunner.TestResults.getFailed());
		Log.colorizeErr(ConsoleStyle.Reset.RESET_ALL);
		if (TestRunner.TestResults.noFails()) {
			Log.flush();
			Log.colorizeOut(ConsoleStyle.Foreground.GREEN);
			Log.outPrintln("all tests passed");
			Log.colorizeOut(ConsoleStyle.Reset.RESET_ALL);
		} else {
			Log.colorizeErr(ConsoleStyle.Foreground.RED);
			Log.errPrintln("one or more tests failed");
			Log.colorizeErr(ConsoleStyle.Reset.RESET_ALL);
		}
		Log.flush();
	}
	
	public static void main(String[] args) {
		if (Main.hasOption(args, "C", "no-colorize"))
			Log.colorizeOutput = false;
		
		if (!Main.saveFolder.exists())
			Main.saveFolder.mkdir();

		for (TestCase testCase : cases) {
			Log.outPrintln(Log.stamp("running test case") + testCase.getClass().getName());
			Log.flush();
			List<Test> tests = testCase.initTests();
			for (Test test : tests) {
				Log.outPrintln(Log.stamp("running test") + test.getClass().getName());
				Log.flush();
				test.run();
			}
		}
		
		results();
		if (TestResults.noFails())
			System.exit(0);
		else
			System.exit(1);
	}

}
