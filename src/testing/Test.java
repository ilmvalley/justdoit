package testing;

import justDoIt.ConsoleStyle;
import justDoIt.Log;

public abstract class Test {
	
	public abstract void run();
	
	public static void pass(String passMessage) {
		Log.colorizeOut(ConsoleStyle.Foreground.GREEN);
		Log.outPrintln(Log.stamp("test passed") + passMessage);
		Log.colorizeOut(ConsoleStyle.Reset.RESET_ALL);
		Log.flush();
		TestRunner.TestResults.addSuccess();		
	}
	
	public static void fail(String failMessage) {
		Log.colorizeErr(ConsoleStyle.Foreground.RED);
		Log.errPrintln(Log.stamp("test failed") + failMessage);
		Log.colorizeErr(ConsoleStyle.Reset.RESET_ALL);
		Log.flush();
		TestRunner.TestResults.addFail();
	}
	
	public static void fail(String failMessage, Throwable e) {
		Log.errPrintln(Log.stamp("test failed") + failMessage);
		Log.error("", e);
		Log.flush();
		TestRunner.TestResults.addFail();
	}
	
	public void assertTrue(boolean a) {
		assertTrue(a, "assertion passed (got true)", "assertion failed (got false)");
	}
	
	public void assertTrue(boolean a, String passMessage, String failMessage) {
		if (a)
			pass(passMessage);
		else
			fail(failMessage);
	}
	
	public void assertFalse(boolean a) {
		assertFalse(a, "assertion passed (got false)", "assertion failed (got true)");
	}
	
	public void assertFalse(boolean a, String passMessage, String failMessage) {
		if (a)
			fail(failMessage);
		else
			pass(passMessage);
	}
	
	public void assertNull(Object a) {
		assertNull(a, "assertion passed (got null)", "assertion failed (got " + a + ")");
	}
	
	public void assertNull(Object a, String passMessage, String failMessage) {
		if (a == null)
			pass(passMessage);
		else
			fail(failMessage);
	}
	
	public void assertEqual(Object a, Object b) {
		assertEqual(a, b, "assertion passed (equal)", "assertion failed (not equal)");
	}
	
	public void assertEqual(Object a, Object b, String passMessage, String failMessage) {
		if (a == b)
			pass(passMessage);
		else if (a == null || b == null)
			fail(failMessage);
		else if (a.equals(b) && b.equals(a))
			pass(passMessage);
		else
			fail(failMessage);
	}
}
