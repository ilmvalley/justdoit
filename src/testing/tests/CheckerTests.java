package testing.tests;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import interpreter.Checker;
import testing.ParamTest;
import testing.Test;
import testing.TestCase;

public class CheckerTests extends TestCase {
	
	private static Set<String> patterns = new LinkedHashSet<String>();
	private static Checker testC;
	
	private static boolean foundPattern;

	public List<Test> initTests() {
		List<Test> tests = new LinkedList<>();
		//a pattern
		patterns.add("test %%num%%");
		testC = new Checker(patterns, Set.of(Set.of("test2","test"))) {
			public boolean process(String pattern, Map<String, String> params) {
				foundPattern = true;
				return true;
			}
		};
		tests.add(new ParamTest<String>("test 2", "test 3", "test2 test") {
			public void beforeTest() {
				foundPattern = false;
			}
			
			public void test(String param) {
				testC.check(param);
				assertTrue(foundPattern, "\"" + param + "\" matched", "\"" + param + "\" didn't match");
			}
		});
		tests.add(new ParamTest<String>("test", "test3 3", "test3", "not a match") {
			public void beforeTest() {
				foundPattern = false;
			}
			
			public void test(String param) {
				testC.check(param);
				assertFalse(foundPattern, "\"" + param + "\" didn't match", "\"" + param + "\" matched");
			}
		});
		tests.add(new Test() {
			public void run() {
				foundPattern = false;
				patterns.add("add");
				testC.check("add"); //should pass
				assertTrue(foundPattern, "added pattern matched","added pattern didn't match");
			}
		});
		tests.add(new Test() {
			public void run() {
				foundPattern = false;
				patterns.add("add");
				testC.check("added"); //shouldn't pass
				assertFalse(foundPattern, "not added pattern didn't match","not added pattern matched");
			}
		});
		return tests;
	}
}
