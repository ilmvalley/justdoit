package testing.tests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;

import justDoIt.Log;
import justDoIt.ResourceLoader;
import justDoIt.ResourceLoader.ResourceNotFoundException;
import testing.Test;
import testing.TestCase;

public class ResourceTests extends TestCase {
	
	public class DirectoryTest extends Test {
		public void run() {
			boolean isDir;
			try {
				isDir = ResourceLoader.global.isDirectory("justDoIt", null);
			} catch (ResourceNotFoundException e) {
				fail("AAS patterns were not found",e);
				return;
			}
			assertTrue(isDir,"/justDoIt/ is a folder","/justDoIt is not a folder");
		}
	}
	
	public class NotDirectoryTest extends Test {
		public void run() {
			boolean isDir;
			try {
				isDir = ResourceLoader.global.isDirectory("test.txt", ResourceLoader.class);
			} catch (ResourceNotFoundException e) {
				fail("test.txt was not found",e);
				return;
			}
			assertFalse(isDir,"test.txt is not a folder","test.txt is a folder");
		}
	}
	
	public class TestFileTest extends Test {
		public void run() {
			Reader testReader = null;
			try {
				testReader = ResourceLoader.global.getReader("test.txt", ResourceLoader.class);
			} catch (IOException e) {
				Log.error("error while trying to fetch test.txt", e);
			}
			BufferedReader testBuf = new BufferedReader(testReader);
			try {
				assertEqual(testBuf.readLine(),"file for starting/testing the resource loader","first line of \"test.txt\" line matches","first line of \"test.txt\" is different");
				assertNull(testBuf.readLine(),"end of \"test.txt\" reached","end of \"test.txt\" not reached");
			} catch (IOException e) {
				Log.error("error while trying to read test.txt",e);
			}
		}
	}
	
	public class NotFileTest extends Test {
		public void run() {
			try {
				ResourceLoader.global.isDirectory("something that doesn't exist", ResourceTests.class);
			} catch (ResourceNotFoundException e) {
				pass(e.getMessage());
				return;
			}
			fail("could get invalid resource file");
		}
	}
	
	public class ForEachTest extends Test {
		private boolean failed = false;
		private boolean found = false;
		
		public void run() {
			ResourceLoader.global.forEachInFolder("testing", f -> {
				Log.verbose("testing folder contains: " + f);
				if (failed)
					return;
				found = true;
				if (f.contains("ResourceTets")) {
					fail("found content of a subfolder to testing: " + f);
					failed = true;
				}
			}, null);
			if (found) {
				if (!failed)
					pass("couldn't get subfolder contents of testing");
			} else
				fail("couldn't get folder contents of testing");
		}
	}
	
	public List<Test> initTests() {
		return List.of(
				new DirectoryTest(),
				new NotDirectoryTest(),
				new TestFileTest(),
				new NotFileTest(),
				new ForEachTest()
			);
	}

}
