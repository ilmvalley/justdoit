package testing.tests;

import java.io.File;
import java.util.List;

import interpreter.ObjectStore;
import justDoIt.Main;
import testing.Test;
import testing.TestCase;

public class ObjectStoreTests extends TestCase {
	
	private String storageFilePath = Main.saveFolder.getPath() + File.separator + "testing";
	private ObjectStore<String> store = new ObjectStore<>("testing");
	
	public class StorageTest extends Test {
		public void run() {
			List<String> testList = List.of("test","test2", "test3");
			store.saveList(testList);
			assertTrue(new File(storageFilePath).exists(),"storage file exists","storage file doesn't exist");
			assertTrue(store.loadVector(String.class).equals(testList),"list was saved and loaded","list couldn't be saved and/or loaded");
			store.delete();
			assertFalse(new File(storageFilePath).exists(),"storage file seems deleted", "storage file wasn't deleted");
		}
	}
	
	public List<Test> initTests() {
		return List.of(new StorageTest());
	}

}
