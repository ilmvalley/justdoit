package testing;

import java.util.List;

public abstract class ParamTest<T> extends Test {

	private List<T> parameterList;

	@SafeVarargs
	public ParamTest(T... params) {
		parameterList = List.of(params);
	}
	
	public ParamTest(List<T> params) {
		parameterList = params;
	}

	public void beforeTest() {
		
	}
	
	public void run() {
		for (T param : parameterList) {
			beforeTest();
			test(param);
		}
	}
	
	public abstract void test(T param);

}
