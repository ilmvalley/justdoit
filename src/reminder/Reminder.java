package reminder;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.TemporalAmount;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import interpreter.Checker;
import interpreter.ObjectStore;
import justDoIt.Log;
import reminder.Repetition;

/**
 * a task that has to be finished until a certain deadline
 */
public class Reminder implements Serializable {
	
	private static final long serialVersionUID = 8766362773962969513L;

	/**
	 * all scheduled reminders
	 */
	public static List<Reminder> reminders;
	
	/**
	 * the storage file for the reminders
	 */
	public static ObjectStore<Reminder> reminderStore = new ObjectStore<>("reminders");
	
	public static Timer reminderTimer = new Timer("JustDoIt_reminder", true);
	
	/**
	 * instant when the task should be finished
	 */
	public Instant due;
	
	/**
	 * the amount of time to start reminding before the deadline
	 */
	public TemporalAmount remindBefore;
	
	/**
	 * the description of the task
	 */
	public String label;
	
	/**
	 * when the reminder should be repeated
	 */
	public Repetition repetition;
	
	/**
	 * whether the task has been finished
	 */
	private boolean finished = false;
	
	public Reminder(String label, TemporalAmount after) {
		this.label = label;
		this.due = Instant.now().plus(after);
		remindBefore = Duration.ZERO;
		repetition = null;
		startTimer();
		reminders.add(this);
	}
	
	public int getId() {
		return reminders.indexOf(this);
	}
	
	/**
	 * @return
	 *   the instant when the bot should start reminding the user
	 */
	public Instant remindTime() {
		return due.minus(remindBefore);
	}
	
	public void startTimer() {
		reminderTimer.schedule(new TimerTask() {
			public void run() {
				remindUser();
			}
		}, Date.from(remindTime()));
	}

	/**
	 * load all reminders and check which are due next
	 */
	public static void load() {
		//load reminders from the file
		reminders = reminderStore.loadVector(Reminder.class);
		Log.debug("loaded " + reminders.size() + " reminders");
		for (Reminder r : reminders) {
			if (r.finished)
				continue;
			
			if (r.remindTime().isBefore(Instant.now())) {
				r.remindUser();
			} else {
				r.startTimer();
			}
		}
	}
	
	/**
	 * save all reminders
	 */
	public static void save() {
		reminderStore.saveList(reminders);
	}

	private void remindUser() {
		ZonedDateTime zonedDue = due.atZone(ZoneId.systemDefault());
		DateTimeFormatter format = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
		String formatedDue = zonedDue.format(format);
		Checker.answer("Reminder " + getId() + " \"" + label + "\" is due " + formatedDue);
	}
	
	public boolean isFinished() {
		return finished;
	}

	/**
	 * finish a task
	 * @return
	 *   whether the reminder was repeated
	 */
	public boolean finish() {
		if (repetition == null) {
			finished = true;
			return false;
		} else {
			due = repetition.calcNextRepetition(due);
			return true;
		}
	}
}
