package reminder;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Iterator;
import java.util.Set;

/**
 * a class showing when to repeat a reminder
 */
public class Repetition {

	public long months;
	public long weeks;
	public Set<DayOfWeek> days;
	
	/**
	 * create a new repetition
	 * @param months
	 *   how many months should be between the repetitions
	 * @param weeks
	 *   how many weeks should be between the repetitions
	 * @param days
	 *   on which days the reminder should appear
	 */
	public Repetition(int months, int weeks, Set<DayOfWeek> days) {
		this.months = months;
		this.weeks = weeks;
		this.days = days;
	}

	/**
	 * calculate the next repetition
	 * @param date
	 *   the instant the next repetition should be after
	 * @return
	 *   the next instant following the repetitions rules 
	 */
	public Instant calcNextRepetition(Instant date) {
		//add up the months
		Instant next = date.plus(months, ChronoUnit.MONTHS);
		//add up the weeks
		next = date.plus(weeks, ChronoUnit.WEEKS);
		//add up the days to the next possible day of week
		next = next.plus(getDaysToNext(date.get(ChronoField.DAY_OF_WEEK)),ChronoUnit.DAYS);
		//return the result
		return next;
	}

	/**
	 * find the next DayOfWeek in the days set
	 * @param start
	 *   the starting day
	 * @return
	 *   the next DayOfWeek after the starting day
	 */
	private DayOfWeek getNextDay(DayOfWeek start) {
		//find the day after the starting day
		for (DayOfWeek day : days)
			if (day.getValue() > start.getValue())
				return day;
		//no next day found in this week
		Iterator<DayOfWeek> it = days.iterator();
		//return the first day of the next week
		return it.next();
	}

	/**
	 * get the day count to the next given day in the days set
	 * @param startDay
	 *   the index of the day the next should be after
	 * @return
	 *   the amount of days to the next possible weekday
	 */
	private long getDaysToNext(int startDay) {
		//get the next weekday
		DayOfWeek next = getNextDay(DayOfWeek.of(startDay));
		//check if its in the next week
		if (next.getValue()<=startDay)
			return 7 + next.getValue() - startDay;
		else
			return next.getValue() - startDay;
	}
}
