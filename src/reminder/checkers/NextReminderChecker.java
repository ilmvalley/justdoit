package reminder.checkers;

import java.time.Instant;
import java.util.Map;
import java.util.Set;

import interpreter.Checker;
import justDoIt.Log;
import reminder.Reminder;

public class NextReminderChecker extends Checker {
	
	private static final Set<String> activationPatterns = Set.of(
			"Which reminder is due next?",
			"What is due next?"
	);
	
	public NextReminderChecker() {
		super(activationPatterns);
	}

	public boolean process(String pattern, Map<String, String> params) {
		if (pattern.contains("next")) {
			Instant max = Instant.MAX;
			Reminder first = null;
			for (Reminder r : Reminder.reminders) {
				if (!r.isFinished())
					if (r.due.isBefore(max)) {
						first = r;
						max = r.due;
					}
			}
			if (first != null) {
				answer("Reminder " + first.getId() + " \"" + first.label + "\" is the earliest reminder.");
			}
			return true;
		} else {
			Log.warning("unrecognized pattern " + pattern);
			return false;
		}
	}

}
