package reminder.checkers;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import interpreter.Checker;
import justDoIt.Log;
import reminder.Reminder;

public class ReminderActions extends Checker {
	
	private static final Set<String> finishPatterns = Set.of(
			"I finished task %%id%%",
			"task %%id%% is done"
	);
	private static final List<String> finishAnswers = List.of(
			"Congrats!", "Great!"
	);
	
	private static final Set<String> deletePatterns = Set.of(
			"delete task %%id%%"
	);
	private static final List<String> deleteAnswers = List.of(
			"Deleted!"
	);
	
	private static final Set<String> activationPatterns = new LinkedHashSet<>();
	private static final Set<Set<String>> wordAliases = Set.of();
	private static boolean inited = false;
	
	
	public ReminderActions() {
		super(activationPatterns,wordAliases);
		init();
	}

	private void init() {
		if (inited) return;
		activationPatterns.addAll(finishPatterns);
		activationPatterns.addAll(deletePatterns);
		inited = true;
	}

	public boolean process(String pattern, Map<String, String> params) {
		int id = Integer.parseInt(params.get("id"));
		if (finishPatterns.contains(pattern)) {
			Reminder.reminders.get(id).finish();
			answerRandom(finishAnswers);
			return true;
		} else if (deletePatterns.contains(pattern)) {
			//TODO: ask if user is sure (we need talking context for this)
			Reminder.reminders.remove(id);
			answerRandom(deleteAnswers);
			return true;
		} else {
			Log.warning("unrecognized pattern: " + pattern);
			return false;
		}
	}
}
