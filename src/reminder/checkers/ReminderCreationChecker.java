package reminder.checkers;

import java.time.Duration;
import java.util.Map;
import java.util.Set;
import interpreter.Checker;
import justDoIt.Log;
import reminder.Reminder;

/**
 * the checker to handle reminders
 */
public final class ReminderCreationChecker extends Checker {

	/**
	 * the patterns that activate the checker
	 */
	private static Set<String> activationPatterns = Set.of(
		"remind me in %%seconds%% seconds to %%action%%",
		"remind me in %%minutes%% minutes to %%action%%",
		"remind me in %%hours%% hours to %%action%%",
		"remind me in %%days%% days to %%action%%"
		//TODO: multiple time units, setting exact time and date, removing reminders, finishing tasks, etc.
	);
	
	/**
	 * the words that can be replaced with equal meaning
	 */
	private static Set<Set<String>> wordAliases = Set.of(
		Set.of("remind", "tell"),
		Set.of(" seconds", "seconds", " second", "second", " sec", "sec"),
		Set.of(" minutes", "minutes", " minute", "minute", " min", "min"),
		Set.of(" hours", "hours", " hour", "hour", " h", "h"),
		Set.of(" days", "days", " day", "day", " d", "d")
	);
	
	/**
	 * create the checker
	 */
	public ReminderCreationChecker() {
		super(activationPatterns,wordAliases);
	}

	/**
	 * the processor for received messages
	 * @see interpreter.Checker#process(java.lang.String, java.util.LinkedHashMap)
	 */
	public boolean process(String pattern, Map<String, String> params) {
		//the default time until reminding
		Duration after = Duration.ZERO;
		after = after.plus(Duration.ofSeconds(Long.parseLong(params.getOrDefault("seconds","0"))));
		after = after.plus(Duration.ofMinutes(Long.parseLong(params.getOrDefault("minutes","0"))));
		after = after.plus(Duration.ofHours(Long.parseLong(params.getOrDefault("hours","0"))));
		after = after.plus(Duration.ofDays(Long.parseLong(params.getOrDefault("days","0"))));
		
		if (after.isNegative()) {
			Log.info("user did stupid stuff: time is negative");
			answer("Please enter a time that is in the future.");
			return true;
		}
		
		new Reminder(params.get("action"), after);
		
		//yay
		Log.action("reminder set!");
		answer("Ok, I'll do that!");
		return true;
	}
}