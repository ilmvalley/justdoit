package interpreter;

import java.util.LinkedList;
import java.util.List;
import reminder.checkers.NextReminderChecker;
import reminder.checkers.ReminderActions;
import reminder.checkers.ReminderCreationChecker;
import interpreter.aas.AAS;
import interpreter.checkers.HowDoIDo;
import interpreter.checkers.StartupCommands;
import interpreter.checkers.SystemCommands;
import interpreter.checkers.TestChecker;
import interpreter.checkers.TimeChecker;

/**
 * the processor that triggers all its checkers when it receives a message
 */
public final class MessageProcessor {
	
	/**
	 * the checkers that should be triggered when a message is received
	 */
	private static final List<Checker> checkers = new LinkedList<>();
	public static final String helpTheDevs = "You can give the developers a hint by telling them what you want the chatbot to understand in an issue or on the chat.";
	private static boolean inited = false;
	
	/**
	 * a method initializing the message processor,
	 * adds the checkers that should be checked by default
	 */
	public static void init() {
		if (inited) return;
		checkers.add(new TestChecker());
		checkers.add(new TimeChecker());
		checkers.add(new ReminderCreationChecker());
		checkers.add(new ReminderActions());
		checkers.add(new NextReminderChecker());
		checkers.add(new SystemCommands());
		checkers.add(AAS.globalAAS);
		checkers.add(new HowDoIDo());
		checkers.add(new StartupCommands());

		inited = true;
	}
	
	/**
	 * let all checkers check the message
	 * @param text
	 *   the raw text of the message received
	 */
	public static void msg(String text) {
		init();
		boolean found = false;
		for (Checker ch : checkers)
			if (ch.check(text))
				found = true;
		if (!found) {
			if (text.startsWith("::")) {
				Checker.answer("::not_found");
			} else {
				Checker.answerRandom(List.of("I don't understand that.","I don't know what you're trying to say."));
				Checker.answer(helpTheDevs);
			}
		}
	}
	
	public static String getDescription(String pattern) {
		for (Checker ch : checkers)
			if (ch instanceof CommandChecker) {
				String desc = ((CommandChecker) ch).getDescription(pattern);
				if (desc != null)
					return desc;
			}
		return "command " + pattern;
	}
}
