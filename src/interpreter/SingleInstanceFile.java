package interpreter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import justDoIt.Log;
import justDoIt.Main;

/**
 * a class handling a property store file
 */
public class SingleInstanceFile {
	/**
	 * a property storage containing all the values
	 */
	private static Properties storage = new Properties();
	/**
	 * the file the property storage is saved in
	 */
	private static File storageFile = new File(Main.saveFolder.getPath() + File.separator + "runningStore");
	
	/**
	 * load the properties from the file
	 */
	public static synchronized void load() { 
		//if it's there
		if (storageFile.exists()) {
			//variable for the input stream
			FileInputStream fin = null;
			try {
				//create an input stream
				fin = new FileInputStream(storageFile);
				//load the properties
				storage.load(fin);
			} catch (IOException e) {
				//oh no something went wrong
				Log.error("couldn't load properties file: "+storageFile.getName(), e);
			} finally {
				try {
					//at least close the stream
					fin.close();
				} catch (IOException e) {
					//hmm more error
					Log.error("couldn't close property file: "+storageFile.getName(), e);
				}
			}
		}
	}

	/**
	 * save the properties to the file
	 */
	public static synchronized void save() {
		//remove the file
		if (storageFile.exists()) {
			storageFile.delete();
		}
		FileOutputStream fout = null;
		try {
			//new file
			storageFile.createNewFile();
			//create an output stream
			fout = new FileOutputStream(storageFile);
			//save the properties
			storage.store(fout,"JustDoIt storage");
		} catch (IOException e) {
			//something went wrong
			Log.error("couldn't save properties to file: "+storageFile.getName(), e);
		} finally {
			try {
				//try to close the stream
				fout.close();
			} catch (IOException e) {
				// more error
				Log.error("couldn't close properties file: "+storageFile.getName(), e);
			}
		}
	}

	/**
	 * get a property
	 * @param key
	 *   the property's name
	 * @return
	 *   the property's value or null if not present
	 */
	public static String get(String key) {
		return storage.getProperty(key);
	}
	
	/**
	 * get a property
	 * @param key
	 *   the property's name
	 * @param defaultValue
	 *   the value to be returned if the property is not present
	 * @return
	 *  the property's value or the default
	 */
	public static String get(String key, String defaultValue) {
		String prop = storage.getProperty(key);
		if (prop==null) {
			return defaultValue;
		}
		return prop;
	}

	/**
	 * set a property
	 * @param key
	 *   the property's name
	 * @param value
	 *   the property's new value
	 */
	public static void set(String key, String value) {
		storage.setProperty(key, value);
	}

	public static void delete() {
		Log.warning("deleting Storage");
		storageFile.delete();
	}

}
