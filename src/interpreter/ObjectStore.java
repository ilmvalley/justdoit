package interpreter;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Vector;

import justDoIt.Log;
import justDoIt.Main;

/**
 * a store that can save objects into a file
 * @param <T>
 *   the type of objects inside the file
 */
public class ObjectStore<T> {
	/**
	 * the file where the data is saved
	 */
	private final File storeFile;

	/**
	 * @param filename
	 *   the name or path of the file the objects are saved to
	 */
	public ObjectStore(String filename) {
		storeFile = new File(Main.saveFolder.getPath() + File.separator + filename);
	}
	
	/**
	 * load all objects from the file
	 * @param c
	 *   the class of the objects inside the files
	 * @return
	 *   a Vector containing all objects
	 */
	public Vector<T> loadVector(Class<T> c) {
		//log it
		Log.action("load from file " + storeFile.getName());
		//create a file input stream
		FileInputStream fis;
		try {
			//try to open the store file
			fis = new FileInputStream(storeFile);
		} catch (FileNotFoundException e) {
			//it's not there
			Log.warning("Couldn't find file " + storeFile.getName());
			//no objects loaded
			return new Vector<>();
		}
		//create an object input stream
		ObjectInputStream ois = null;
		try {
			//pipe from the file
			ois = new ObjectInputStream(fis);
		} catch (IOException e) {
			//shoot it's not working
			Log.error("Couldn't get object input from file " + storeFile.getName(), e);
			try {
				//free the file and memory
				fis.close();
			} catch (IOException e1) {
				//unclosed or closed before
				Log.error("Couldn't close file " + storeFile.getName(), e1);
			}
			//no objects loaded
			return new Vector<>();
		}
		//create the output vector
		Vector<T> output = new Vector<>();
		//a variable for the loaded object
		Object o;
		try {
			//if read and add objects if they are of type c
			while ((o = ois.readObject()) != null)
				if (c.isInstance(o))
					output.add(c.cast(o));
		} catch (ClassNotFoundException e) {
			//this is either not ours or old
			Log.error("Didn't recognize class read from file " + storeFile.getName(), e);
		} catch (EOFException e) {
			//reached bottom of the file
			Log.info("end of file " + storeFile.getName());
		} catch (IOException e) {
			//shoot, something went wrong with reading
			Log.error("Couldn't read file " + storeFile.getName(), e);
		}
		try {
			//free the file and memory
			ois.close();
		} catch (IOException e) {
			//unclosed or closed before
			Log.error("Couldn't close the input stream for file " + storeFile.getName(),e);
		}
		//this is what we got
		return output;
	}
	
	public void saveList(List<T> v) {
		//log it
		Log.action("save to file " + storeFile.getName());
		//create a file output stream
		FileOutputStream fos = null;
		try {
			//try to open the store file
			fos = new FileOutputStream(storeFile);
		} catch (FileNotFoundException e) {
			//it's not there
			Log.error("Couldn't create file " + storeFile.getName(), e);
			//can't save
			return;
		}
		//create an object output stream
		ObjectOutputStream oos = null;
		try {
			//pipe to the file
			oos = new ObjectOutputStream(fos);
		} catch (IOException e) {
			//shoot it's not working
			Log.error("Couldn't create object output for file " + storeFile.getName(), e);
			try {
				//free the file and memory
				fos.close();
			} catch (IOException e1) {
				//unclosed or closed before
				Log.error("Couldn't close file " + storeFile.getName(), e1);
			}
			//couldn't save
			return;
		}
		try {
			//write all objects in the vector
			for (T o : v)
				oos.writeObject(o);
		} catch (IOException e) {
			//shoot, something went wrong with writing
			Log.error("Coudln't write to file " + storeFile.getName(), e);
		}
		try {
			//free the file and memory
			oos.close();
		} catch (IOException e) {
			//unclosed or closed before
			Log.error("Couldn't close the output stream for file " + storeFile.getName(), e);
		}
	}

	/**
	 * save all objects to the file overwriting the existing file
	 * @param v
	 *   a vector containing all objects to be saved
	 */
	public void saveVector(Vector<T> v) {
		saveList(v);
	}

	public void delete() {
		Log.warning("deleting object store: " + storeFile.getName());
		storeFile.delete();
	}
}
