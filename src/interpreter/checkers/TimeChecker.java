package interpreter.checkers;

import java.time.LocalTime;
import java.util.Map;
import java.util.Set;

import interpreter.Checker;

/**
 * a checker handling messages asking for the time
 */
public final class TimeChecker extends Checker {

	/**
	 * all patterns that ask for the time
	 */
	private static final Set<String> activationPatterns = Set.of("What time is it?","tell me the time","How late is it?","What's the time?");
	
	/**
	 * these are pretty equal
	 */
	private static final Set<Set<String>> wordAliases = Set.of(
			Set.of("What's", "What is")
	);

	/**
	 * build the checker
	 */
	public TimeChecker() {
		super(activationPatterns,wordAliases);
	}

	/**
	 * a processor answering with the current time
	 * @see interpreter.Checker#process(java.lang.String, java.util.LinkedHashMap)
	 */
	public boolean process(String pattern, Map<String, String> params) {
		answer(LocalTime.now().toString());
		return true;
	}

}
