package interpreter.checkers;

import java.util.Map;
import java.util.Set;

import interpreter.Checker;

/**
 * a checker for a few testing messages
 */
public final class TestChecker extends Checker {

	/**
	 * the patterns that activate the process function
	 */
	private static final Set<String> testPatterns = Set.of("test counting to %%number%%","test counting", "test");
	
	/**
	 * build the checker
	 */
	public TestChecker() {
		super(testPatterns);
	}

	/**
	 * the processor for the test messages
	 * @see interpreter.Checker#process(java.lang.String, java.util.LinkedHashMap)
	 */
	public boolean process(String pattern, Map<String, String> params) {
		//if it's the counting pattern or any related
		if (pattern.startsWith("test counting")) {
			//the default number to count to
			int number = 3;
			//if we got a number
			if (params.get("number")!=null)
				try {
					//try and get the number from the string
					number = Integer.valueOf(params.get("number"));
				} catch (NumberFormatException e) {
					//error in front of keyboard
					answer("\""+params.get("number")+"\"is not a number");
					return true;
				}
			//let's count up to the given number
			if (number < 1)
				answer("test counting is only for countig upwards from 1");
			for (int i = 1; i <= number; i++)
				answer(String.valueOf(i));
			return true;
		} else if (pattern.equals("test")) {
			//display all the test patterns
			answer("avalible tests:\n" + String.join("\n", testPatterns));
			return true;
		}
		return false;
	}
}
