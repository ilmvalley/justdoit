package interpreter.checkers;

import java.util.Map;
import java.util.Set;

import interpreter.Checker;
import interpreter.MessageProcessor;
import justDoIt.Log;

public final class HowDoIDo extends Checker {

	public HowDoIDo() {
		super(Set.of("How do I do?", "How do I don't?", "How do I do %%stuff%% ?"));
	}

	public boolean process(String pattern, Map<String, String> params) {
		String stuff = params.get("stuff");
		
		if (pattern.equals("How do I do?")) {
			answer("I don't know but perhaps you can ask for something else starting with \"How do I do\"");
			return true;
		} else if (pattern.equals("How do I don't?")) {
			answer("Just don't ...");
			return true;
		} else if (stuff.equals("bread")) {
			answer("Recipe for bread: I think it was like 4 cups of flour and 1 or 2 cups of water idk tbh. Make dough with that, put it on a tablet and then put it in the oven for a while." );
			return true;
		} else {
			answer("dunno");
			Log.debug("didn't know how to do \"" + params.get("stuff") + '"');
			Checker.answer(MessageProcessor.helpTheDevs);
			return true;
		}
	}
}
