package interpreter.checkers;

import java.util.Map;

import interpreter.CommandChecker;
import justDoIt.Log;

public class StartupCommands extends CommandChecker {
	
	public StartupCommands() {
		super(Map.of("::init", "beginning of chat", "::open", "user entered chat", "::startup","bot entered chat", "::quit", "bot left chat", "::not_found", "command not found"));
	}

	public boolean process(String pattern, Map<String, String> params) {
		//TODO
		Log.warning("Startup commands shouldn't be usable.");
		return true;
	}

}
