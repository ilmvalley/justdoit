package interpreter.checkers;

import java.util.Map;
import interpreter.CommandChecker;
import interpreter.SingleInstanceFile;
import justDoIt.Log;
import justDoIt.Main;
import reminder.Reminder;

public class SystemCommands extends CommandChecker {
	
	public SystemCommands() {
		super(Map.of("::exit", "", "::delete data", "warning: content should have been deleted", "::close", "user left chat"));
	}
	
	public boolean process(String pattern, Map<String, String> params) {
		switch (pattern) {
		case "::exit":
			System.exit(0);
			return true;
		case "::delete data":
			Main.messageStore.delete();
			Reminder.reminderStore.delete();
			SingleInstanceFile.delete();
			Main.exitWithoutSave();
			return true;
		case "::close":
			Main.chat.dispose();
			return true;
		default:
			Log.warning("unknown system command: " + pattern);
			return false;
		}
	}

}
