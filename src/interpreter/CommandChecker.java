package interpreter;

import java.util.Map;
import java.util.Set;

public abstract class CommandChecker extends Checker {
	
	protected Map<String, String> commands;
	
	public CommandChecker(Map<String, String> commands) {
		super(commands.keySet());
		this.commands = commands;
	}

	public CommandChecker(Map<String, String> commands, Set<Set<String>> aliases) {
		super(commands.keySet(), aliases);
		this.commands = commands;
	}
	
	protected void updatePatternSet() {
		activationPatterns = commands.keySet();
	}
	
	public String getDescription(String pattern) {
		return commands.get(pattern);
	}
}
