package interpreter.aas;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import justDoIt.Log;
import testing.Test;
import testing.TestCase;

public class AASTest extends TestCase {
	
	private boolean patternFound;
	
	public void findAASPattern(String pattern, Map<String, String> params) {
		AAS.getPatterns().forEach(pat -> {
			if (!pat.bases.contains(pattern))
				return;
			Log.info("found AAS pattern with the right base");
			List<String> ans = pat.getAnswers(params);
			if (ans.isEmpty())
				return;
			Log.debug("found AAS pattern matching the message");
			patternFound=true;
		});
	}
	
	public List<Test> initTests() {
		List<Test> tests = new LinkedList<>();
		
		tests.add(new Test() {
			public void run() {
				patternFound = false;
				findAASPattern("AAS test",Map.of());
				assertTrue(patternFound,"found \"AAS test\"","\"AAS test\" didn't match");
			}
		});
		
		tests.add(new Test() {
			public void run() {
				patternFound = false;
				findAASPattern("AAS test",Map.of("test","test"));
				assertTrue(patternFound,"found \"AAS test test\"","\"AAS test test\" didn't match");
			}
		});
		
		tests.add(new Test() {
			public void run() {
				patternFound = false;
				findAASPattern("AAS test",Map.of("test","not a test"));
				assertFalse(patternFound,"\"AAS test not a test\" didn't match","\"AAS test not a test\" matched");
			}
		});
		
		return tests;
	}

}
