package interpreter.aas;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import interpreter.Checker;
import justDoIt.Log;
import justDoIt.ResourceLoader;
import justDoIt.ResourceLoader.ResourceNotFoundException;

public final class AAS extends Checker {
	
	class Pattern {
		
		private class ParamAnswers {
			
			private Map<String,String> params = new LinkedHashMap<>();
			
			private final List<String> answers = new LinkedList<>();

			public void addAnswer(String answer) {
				answers.add(answer);
			}

			public void addParameter(String parameter, String value) {
				params.put(parameter, value);
			}

			public boolean hasParams(Map<String, String> params2) {
				return params.equals(params2);
			}
			
			public List<String> getAnswers() {
				return answers;
			}
			
			public String toString() {
				return this.getClass().getSimpleName() + "[params="+params+",answers="+answers+"]";
			}
			
		}

		public final Set<String> bases = new LinkedHashSet<>();
		
		private Set<ParamAnswers> params = new LinkedHashSet<>();
		
		public Pattern(String base) {
			bases.add(base);
		}

		public ParamAnswers addParameters() {
			ParamAnswers param = new ParamAnswers();
			params.add(param);
			return param;
		}

		public List<String> getAnswers(Map<String, String> params2) {
			List<String> answers = new LinkedList<>();
			params.forEach(paramAnswers -> {
				if (paramAnswers.hasParams(params2))
					answers.addAll(paramAnswers.getAnswers());
			});
			return answers;
		}
		
		public String toString() {
			return this.getClass().getSimpleName()+"[bases="+bases+",params="+params+"]";
		}

		public void addBase(String base) {
			bases.add(base);
		}

	}

	private static Set<String> activationPatterns = new LinkedHashSet<>();
	private static Set<Pattern> patterns = new LinkedHashSet<>();
	public static final AAS globalAAS = new AAS();
	private AAS() {
		super(activationPatterns);
		Log.action("loading AAS folder");
		try {
			loadRecursive("patterns");
		} catch (EOFException | ResourceNotFoundException e) {
			Log.error("couldn't load AAS folder", e);
		}
		updatePatterns();
		//use this command to see a big message
		//in fact it's for listing all registered patterns
		activationPatterns.add("::AAS list");
		Log.debug("AAS initialized");
	}
	
	private void updatePatterns() {
		patterns.forEach(e -> {
			activationPatterns.addAll(e.bases);
		});
	}
	
	private void loadRecursive(String file) throws EOFException, ResourceNotFoundException {
		Reader resourceReader;
		try {
			resourceReader = ResourceLoader.global.getReader(file, AAS.class);
		} catch (IOException e1) {
			Log.error("error while getting resource: " + file, e1);
			return;
		}
		BufferedReader reader = new BufferedReader(resourceReader);
		String line;
		try {
			line = reader.readLine();
		} catch (IOException e) {
			Log.error("error wilie loading resource: " + file, e);
			return;
		}
		if (line == null) {
			throw new EOFException("reader for resource file \"" + file + "\" starts with null");
		}
		if (line.startsWith("?AAS")) {
			Log.debug("loading AAS File " + file + line);
				loadAASFile(reader);
			return;
		} else if (ResourceLoader.global.isDirectory(file, AAS.class)) {
			while (line != null) {
				try {
					loadRecursive(file+"/"+line);
				} catch (EOFException e) {
					Log.error("resource file " + file + "seems to be empty or doesn't exist", e);
				}
				try {
					line = reader.readLine();
				} catch (IOException e) {
					Log.error("error while loading resource folder: " + file, e);
				}
			}
		} else {
			Log.error("file " + file + " does not start with \"?AAS\"");
		}
	}

	private void loadAASFile(BufferedReader reader) {
		String line = "";
		Pattern pattern = null;
		Pattern.ParamAnswers params = null;
		do {
			if (line.isEmpty())
				Log.verbose("empty line");
			else if (line.startsWith("/"))
				Log.verbose("comment: " + line.substring(1));
			else if (line.startsWith("#"))
				patterns.add(pattern = new Pattern(line.substring(1)));
			else if (line.startsWith("+"))
				pattern.addBase(line.substring(1));
			else if (line.startsWith("*")) {
				Log.verbose("new param Set: " + line.substring(1));
				params = pattern.addParameters();
			} else if (line.startsWith("-"))
				params.addAnswer(line.substring(1));
			else if (line.contains("=")) {
				String[] content = line.split("=");
				if (content.length > 2)
					Log.error("malformed parameter line " + line);
				if (content.length == 2)
					params.addParameter(content[0],content[1]);
				else
					params.addParameter(content[0], "");
			} else
				Log.error("malformed line " + line);
			try {
				line = reader.readLine();
			} catch (IOException e) {
				Log.error("couldn't read line from AAS file", e);
			}
		} while (line!=null);
		try {
			reader.close();
		} catch (IOException e) {
			Log.error("couldn't close reader", e);
		}
	}
	
	static Set<Pattern> getPatterns() {
		return patterns;
	}

	public boolean process(String pattern, Map<String, String> params) {
		boolean found = false;
		for (Pattern pat : patterns) {
			if (!pat.bases.contains(pattern))
				continue;
			Log.info("found AAS pattern with the right base");
			List<String> ans = pat.getAnswers(params);
			if (ans == null)
				continue;
			if (ans.isEmpty()) {
				found = true;
				continue;
			}
			Log.debug("found AAS pattern matching the message");
			answerRandom(ans);
			found = true;
		}
		if (pattern == "::AAS list") {
			Log.action("listing all registered AAS patterns and the resulting checker patterns");
			answer(patterns + "\n" + activationPatterns);
			found = true;
		}
		return found;
	}
}
