package interpreter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import interpreter.filter.WordFilter;
import justDoIt.Log;
import ui.WritingCaption;

/**
 * a class checking for certain patterns to recognize a message type
 */
public abstract class Checker {
	
	/**
	 * patterns that will trigger the checker
	 */
	protected Set<String> activationPatterns;

	/**
	 * an array of word groups that contain words which can be exchanged with equal meaning 
	 */
	protected Set<Set<String>> wordAliases;
	
	/**
	 * a constructor to build the base structure of a checker
	 * @param activationPatterns
	 *   patterns that should be in a message to activate the processing,
	 *   use %%[parameter name]%% to define parameters that can be given in the message
	 */
	protected Checker(Set<String> activationPatterns) {
		this(activationPatterns,null);
	}
	
	/**
	 * a constructor to build the base structure of a checker
	 * @param activationPatterns
	 *   patterns that should be in a message to activate the processing,
	 *   use %%[parameter name]%% to define parameters that can be given in the message
	 * @param aliases
	 *   an array of word groups that contain words which can be exchanged with equal meaning
	 */
	protected Checker(Set<String> activationPatterns, Set<Set<String>> aliases) {
		this.activationPatterns = activationPatterns;
		this.wordAliases = aliases;
	}
	
	/**
	 * an easier way for the chat-bot to answer
	 * @param text
	 *   the text the chat-bot should write
	 */
	public static void answer(String text) {
		WritingCaption.write(text);
	}
	
	public static void answerRandom(List<String> answers) {
		int len = answers.size();
		for (int i = 0; i < len; i++) {
			if (Math.random() <= ((double) i / (double) len)) {
				answer(answers.get(i));
				return;
			}
		}
	}
	
	/**
	 * the method that is called when the checker got a message that matched the activation patterns
	 * @param pattern
	 *   the activation pattern that matched the message
	 * @param params
	 *   the parameters given in the message
	 * @return
	 *   whether the pattern was processed
	 */
	public abstract boolean process(String pattern, Map<String, String> params);

	/**
	 * checks the message whether it matches any of the activation patterns
	 * @param msg
	 *   the raw message text
	 * @return 
	 *   whether the pattern matched
	 */
	public boolean check(String msg) {
		String text = WordFilter.global.filter(msg);
		for (String pattern : activationPatterns) {
			//Log.verbose("checking pattern: " + pattern);
			//TODO: split by variables first
			String[] words = pattern.split(" ");
			String lastVariable = null;
			String textRest = text;
			boolean found = true;
			LinkedHashMap<String, String> params = new LinkedHashMap<>();
			for (String word : words) {
				if (word.startsWith("%%") && word.endsWith("%%")) {
					lastVariable = word.substring(2, word.length()-2);
					if (textRest.isEmpty())
						found = false;
				} else {
					FoundWord foundW = indexOfWord(textRest,word);
					if (foundW.idx == -1) {
						found = false;
						break;
					}
					if (lastVariable != null) {
						params.put(lastVariable, textRest.substring(0,foundW.idx));
						lastVariable = null;
						textRest = textRest.substring(foundW.idx + foundW.word.length());
					} else {
						if (foundW.idx != 0) {
							found = false;
							break;
						}
						textRest = textRest.substring(foundW.word.length());
					}
					if (!textRest.isEmpty()) {
						if (textRest.startsWith(" ")) {
							textRest = textRest.substring(1);
						} else {
							found = false;
							break;
						}
					}
				}
			}
			if (lastVariable != null) {
				params.put(lastVariable, textRest);
				textRest = "";
			}
			if (found && textRest.isEmpty()) {
				Log.debug("processing pattern \"" + pattern + "\"");
				return process(pattern,params);
			}
			//Log.verbose("didn't match");
		}
		return false;
	}
	
	/**
	 * check if a word or an alias to it is present
	 * @param text
	 *   the text to be checked for the word
	 * @param word
	 *   the word to be found
	 * @return
	 *   the word looked for and it's position in the text
	 */
	private FoundWord indexOfWord(String text, String word) {
		if (wordAliases==null)
			return new FoundWord(word,text.toLowerCase().indexOf(word.toLowerCase()));
		for (Set<String> aliasS : wordAliases) {
			for (String sAlias : aliasS) {
				if (sAlias.equalsIgnoreCase(word)) {
					//found alias --> check if neighbor aliases can be found in textRest
					int eIdx = -1;
					int max = -1;
					String eAlias = null;
					for (String fAlias : aliasS) {
						int idx = text.toLowerCase().indexOf(fAlias.toLowerCase());
						if (idx != -1) {
							if (fAlias.length() > max) {
								max = fAlias.length();
								eAlias = fAlias;
								eIdx = idx;
							}
						}
					}
					if (eAlias == null) {
						break;						
					} else {
						return new FoundWord(eAlias,eIdx);
					}
				}
			}
		}
		return new FoundWord(word,text.toLowerCase().indexOf(word.toLowerCase()));
	}
}
