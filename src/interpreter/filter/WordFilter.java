package interpreter.filter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import justDoIt.Log;
import justDoIt.ResourceLoader;

public final class WordFilter {
	
	/**
	 * the word filter singleton
	 */
	public static final WordFilter global = new WordFilter();
	
	/**
	 * the words that can be left out without harm to the sentences meaning
	 */
	private Set<WordInSentence> meaninglessWords = new LinkedHashSet<>();
	
	/**
	 * the constructor initializing the WordFilter
	 */
	protected WordFilter() {
		try {
			loadRecursive("meaningless_words");
		} catch (IOException e) {
			Log.error("couldn't load WordFilter resource", e);
		}
	}
	
	/**
	 * loads a word filter file into the word filter
	 * @param file
	 *   the file or folder to be loaded
	 * @throws IOException
	 *   errors that occur while loading
	 */
	private void loadRecursive(String file) throws IOException {
		//check if we got a directory
		if (ResourceLoader.global.isDirectory(file, WordFilter.class)) {
			//get folder contents
			ResourceLoader.global.forEachInFolder(file, e -> {
				try {
					//localize given path
					String path = file;
					//ensure separator at end
					if (!file.endsWith(File.separator))
						path += File.separator;
					//load file in current folder
					loadRecursive(path + e);
				} catch (IOException e1) {
					//oh no!
					Log.error("error while trying to load folder " + e);
				}
			},WordFilter.class);
		} else {
			//read the file
			loadFilterFile(new BufferedReader(ResourceLoader.global.getReader(file, WordFilter.class)));
		}
	}
	
	/**
	 * load a file into the word filter
	 * @param reader
	 *   the reader for the file
	 * @throws IOException
	 *   errors that occur while reading
	 */
	private void loadFilterFile(BufferedReader reader) throws IOException {
		//get the first line
		String type = reader.readLine();
		//check if its a word filter file
		if (!type.startsWith("?WordFilter")) {
			//nope
			Log.warning('"' + type + "\" is not a valid type line for a WordFilter file");
			//don't even try to parse it
			return;
		}
		//OK, got one
		type = type.substring("?WordFilter ".length());
		//check what kind of file it is
		switch (type) {
		case "meaningless":
			//meaningless words
			//read everything
			for (String line = reader.readLine(); line!=null; line = reader.readLine()) {
				//get first char
				char lineStart = line.charAt(0);
				//get rest
				String lineContent = line.substring(1);
				//check which beginning
				switch (lineStart) {
				case '/':
					//comments
					Log.info("comment: "+ lineContent);
					break;
				case '+':
					//sentence start
					meaninglessWords.add(new WordInSentence(lineContent,LocationInSentence.Start));
					break;
				case '-':
					//sentence end
					meaninglessWords.add(new WordInSentence(lineContent,LocationInSentence.End));
					break;
				case '*':
					//anywhere
					meaninglessWords.add(new WordInSentence(lineContent,LocationInSentence.Anywhere));
					break;
				default:
					//something that we don't know yet
					Log.error("unrecognized beginning " + lineStart);
					break;
				}
			}
			break;
		default:
			//something we don't know yet
			Log.warning("WordFilter file of type \"" + type + "\" is not recognized");
			break;
		}
	}

	/**
	 * filter the messages contents
	 * @param msg
	 *   the message to be processed
	 * @return
	 *   the processed message
	 */
	public String filter(String msg) {
		Log.info("filtering message contents");
		String ret = msg;
		boolean found = true;
		while (found) {
			found = false;
			for (WordInSentence meaningless : meaninglessWords) {
				switch (meaningless.location) {
				case Start:
					if (ret.startsWith(meaningless.word)) {
						ret = ret.substring(meaningless.word.length());
						found = true;
					}
					break;
				case Anywhere:
					if (ret.contains(meaningless.word)) {
						ret = ret.replaceAll(meaningless.word, "");
						found = true;
					}
					break;
				case End:
					if (ret.endsWith(meaningless.word)) {
						ret = ret.substring(0, ret.length()-meaningless.word.length());
						found = true;
					}
					break;
				default:
					Log.error("unknown location in sentence: " + meaningless.location);
				}
			}
			ret = ret.strip();
		}
		if (ret.endsWith("."))
			ret = ret.substring(0,ret.length()-1);
		if (ret.contains(".")) {
			//TODO: recognize more accurately and return messages as array
			Log.warning("multiple sentences detected in text: " + ret);
		}
		return ret;
	}
}
