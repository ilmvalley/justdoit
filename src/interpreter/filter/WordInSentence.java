package interpreter.filter;

public class WordInSentence {
	public final String word;
	public final LocationInSentence location;
	
	/**
	 * construct a new sequence that is located somewhere in another string
	 * @param phrase
	 *   the sequence
	 * @param location
	 *   the location of the sequence in the other string
	 */
	public WordInSentence(String phrase, LocationInSentence location) {
		word = phrase;
		this.location = location;
	}
	
	/**
	 * dumps the objects contents into a string
	 * @return
	 *   a string dump of the sequence and the location
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return '"' + word + "\" located: " + location;
	}
}