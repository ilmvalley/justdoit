package justDoIt;

public final class ConsoleStyle {
		public static final class Set {
			public static final int HIGHLIGHT = 1;
			public static final int DIM = 2;
			public static final int ITALICS = 3;
			public static final int UNDERLINE = 4;
			public static final int BLINK = 5;
			public static final int INVERT_FG_BG = 7;
			public static final int HIDE = 8;
			public static final int STRIKETHROUGH = 9;
		}
		public static final class Reset {
			public static final int RESET_ALL = 0;
			public static final int RESET_HIGHLIGHT = 21;
			public static final int RESET_DIM = 22;
			public static final int RESET_UNDERLINE = 24;
			public static final int RESET_BLINK = 25;
			public static final int RESET_INVERT = 27;
			public static final int RESET_HIDDEN = 28;
		}
		public static final class Foreground {
			public static final int BLACK = 30;
			public static final int RED = 31;
			public static final int GREEN = 32;
			public static final int YELLOW = 33;
			public static final int BLUE = 34;
			public static final int MAGENTA = 35;
			public static final int CYAN = 36;
			public static final int LIGHT_GRAY = 37;
			public static final int DEFAULT = 39;
			public static final int DARK_GRAY = 90;
			public static final int LIGHT_RED = 91;
			public static final int LIGHT_GREEN = 92;
			public static final int LIGHT_YELLOW = 93;
			public static final int LIGHT_BLUE = 94;
			public static final int LIGHT_MAGENTA = 95;
			public static final int LIGHT_CYAN = 96;
			public static final int WHITE = 97;
		}
		public static final class Background {
			public static final int DEFAULT = 49;
			public static final int BLACK = 40;
			public static final int RED = 41;
			public static final int GREEN = 42;
			public static final int YELLOW = 43;
			public static final int BLUE = 44;
			public static final int MAGENTA = 45;
			public static final int CYAN = 46;
			public static final int LIGHT_GRAY = 47;
			public static final int DARK_GRAY = 100;
			public static final int LIGHT_RED = 101;
			public static final int LIGHT_GREEN = 102;
			public static final int LIGHT_YELLOW = 103;
			public static final int LIGHT_BLUE = 104;
			public static final int LIGHT_MAGENTA = 105;
			public static final int LIGHT_CYAN = 106;
			public static final int WHITE = 107;
		}
		//public static final int[] YELLOW_BOLD = {BROWN, HIGHLIGHT};
		//public static final int[] PINK_BOLD = {PURPLE, HIGHLIGHT};
}
