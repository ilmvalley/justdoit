package justDoIt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Log {
	//log levels
	public static final int NONE = 0;
	public static final int ERROR = 1;
	public static final int WARNING = 2;
	public static final int ACTION = 3;
	public static final int DEBUG = 4;
	public static final int INFO = 5;
	public static final int VERBOSE = 6;
	
	public static final int ALL = VERBOSE;
	
	/**
	 * the current log level
	 */
	protected static int level = DEBUG;
	
	//the print streams the messages are written to
	protected static PrintStream errStream = System.err;
	protected static PrintStream outStream = System.out;
	protected static PrintStream fileStream = null;
	public static boolean colorizeOutput = true;
	//TODO: separate colorization for each stream
	
	/**
	 * the date-time format to be used for the log time-stamp
	 */
	protected static final DateTimeFormatter stampFormat = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss");
	
	/**
	 * log is not to be used as object
	 */
	protected Log() {}
	
	protected static void colorize(PrintStream stream, int color) {
		if (colorizeOutput)
			stream.print("\u001B["+color+"m");
	}
	
	protected static void colorize(PrintStream stream, int[] colors) {
		if (colorizeOutput)
			for (int color : colors)
				stream.print("\u001B["+color+"m");
	}
	
	/**
	 * generate the date-time, level and thread stamp
	 * @param level
	 *   the log level of the message
	 * @return
	 *   the stamp obviously
	 */
	public static String stamp(String level) {
		//add the date-time stamp
		String stamp = LocalDateTime.now().format(stampFormat) + ": ";
		//add the log level
		stamp += level;
		//add the current thread name
		stamp += "[" + Thread.currentThread().getName() + "]: ";
		//return the stamp
		return stamp;
	}
	
	/**
	 * print to the error stream and to the file
	 * @param text
	 *   the text to be printed
	 */
	public static synchronized void errPrintln(String text) {
		errStream.println(text);
		if (fileStream != null)
			fileStream.println(text);
	}

	/**
	 * print to the output stream and to the file
	 * @param text
	 *   the text to be printed
	 */
	public static synchronized void outPrintln(String text) {
		outStream.println(text);
		if (fileStream != null)
			fileStream.println(text);
	}

	/**
	 * make sure all log messages are written
	 */
	public static void flush() {
		outStream.flush();
		errStream.flush();
		if (fileStream != null)
			fileStream.flush();
	}

	/**
	 * set a new output stream
	 * @param aStream
	 *   the stream
	 */
	public static void setoutStream( PrintStream aStream ) {
		outStream = aStream;
	}
	
	/**
	 * set a new error stream
	 * @param aStream
	 *   the stream
	 */
	public static void seterrStream( PrintStream aStream ) {
		errStream = aStream;
	}
	
	/**
	 * set an output file
	 * @param file
	 *   the file all log messages are written to
	 * @throws FileNotFoundException
	 */
	public static void setLogFile(File file) throws FileNotFoundException {
		if (file == null)
			fileStream = null;
		else
			fileStream = new PrintStream(file);
	}
	
	/**
	 * set the current log level 
	 * @param level
	 *   the level of the most detailed log messages shown
	 */
	public static void setLevel(int level) {
		Log.level = level;
	}

	/**
	 * get the current log level
	 * @return
	 *   the level of the most detailed log messages shown
	 */
	public static int getLevel() {
		return level;
	}

	/**
	 * if something went wrong
	 * @param msg
	 *   a descriptive message of what went wrong
	 */
	public static synchronized void error(String msg) {
		if (level >= ERROR) {
			colorize(errStream,ConsoleStyle.Foreground.RED);
			errPrintln(stamp("ERROR") + msg);
			colorize(errStream,ConsoleStyle.Reset.RESET_ALL);
			flush();
		}
	}
	
	/**
	 * if something went wrong
	 * @param msg
	 *   a descriptive message of what went wrong
	 * @param e
	 *   the throwable that shouldn't have raised
	 */
	public static synchronized void error(String msg, Throwable e) {
		if (level >= ERROR) {
			colorize(errStream,ConsoleStyle.Foreground.RED);
			if (msg=="") {
				errPrintln(stamp("ERROR") + e.getMessage());
			} else {
				errPrintln(stamp("ERROR") + msg + ":  " + e.getMessage());
			}
			e.printStackTrace(errStream);
			if (fileStream != null)
				e.printStackTrace(fileStream);
			colorize(errStream,ConsoleStyle.Reset.RESET_ALL);
			flush();
		}
	}
	
	/**
	 * if something unusual happened
	 * @param msg
	 *   a descriptive message of what was weird
	 */
	public static synchronized void warning(String msg) {
		if (level >= WARNING) {
			colorize(errStream,ConsoleStyle.Foreground.YELLOW);
			errPrintln(stamp("WARNING") + msg);
			colorize(errStream,ConsoleStyle.Reset.RESET_ALL);
			flush();
		}
	}
	
	/**
	 * if something unusual happened
	 * @param msg
	 *   a descriptive message of what was weird
	 * @param e
	 *   the throwable that shouldn't have raised
	 */
	public static synchronized void warning(String msg, Throwable e) {
		if (level >= WARNING) {
			colorize(errStream,ConsoleStyle.Foreground.YELLOW);
			if (msg=="")
				errPrintln(stamp("WARNING") + e.getMessage());
			else
				errPrintln(stamp("WARNING") + msg + ":  " + e.getMessage());
			e.printStackTrace(errStream);
			if (fileStream != null)
				e.printStackTrace(fileStream);
			colorize(errStream,ConsoleStyle.Reset.RESET_ALL);
			flush();
		}
	}

	/**
	 * if something needs to be logged without level
	 * @param msg
	 *   the message written to the log
	 */
	public static synchronized void message(String msg) {
		outPrintln(stamp("") + msg);
		flush();
	}
	
	/**
	 * if something will be done the following messages will refer to
	 * @param msg
	 *   a descriptive message of the process that will begin
	 */
	public static synchronized void action(String msg) {
		if (level >= ACTION) {
			outPrintln(stamp("ACTION") + msg);
			flush();
		}
	}
	
	/**
	 * if something of interest happened
	 * or if the reference of the following messages is split into steps 
	 * @param msg
	 *   a descriptive message containing debug information
	 */
	public static synchronized void debug(String msg) {
		if (level >= DEBUG) {
			outPrintln(stamp("DEBUG") + msg);
			flush();
		}
	}
	
	/**
	 * if something might be worth knowing
	 * @param msg
	 *   the contents of the info
	 */
	public static synchronized void info(String msg) {
		if (level >= INFO) {
			colorize(outStream,ConsoleStyle.Foreground.LIGHT_GRAY);
			outPrintln(stamp("INFO") + msg);
			colorize(outStream,ConsoleStyle.Reset.RESET_ALL);
			flush();
		}
	}
	
	/**
	 * detailed information to see what the program does
	 * @param msg
	 *   the message to be shown
	 */
	public static synchronized void verbose(String msg) {
		if (level >= VERBOSE) {
			colorize(outStream,ConsoleStyle.Foreground.LIGHT_GRAY);
			outPrintln(stamp("VERBOSE") + msg);
			colorize(outStream,ConsoleStyle.Reset.RESET_ALL);
			flush();
		}
	}

	public static void colorizeErr(int style) {
		colorize(errStream,style);
	}
	
	public static void colorizeOut(int style) {
		colorize(outStream,style);
	}
}