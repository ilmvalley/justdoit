package justDoIt;

import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.ProgressMonitor;

import interpreter.MessageProcessor;
import interpreter.ObjectStore;
import interpreter.SingleInstanceFile;
import reminder.Reminder;
import testing.TestRunner;
import ui.Chat;

public final class Main {

	public static final File saveFolder = new File(".JustDoIt");

	/**
	 * the currently open chat window
	 */
	public static Chat chat = null;
	
	/**
	 * the icon shown in the system tray
	 */
	private static TrayIcon trayIcon;
	
	private static SystemTray tray;
	
	private static Thread responseThread;
	
	/**
	 * all the messages that were sent
	 */
	public static Vector<Message> messages;
	
	/**
	 * the storage file for the messages
	 */
	public static ObjectStore<Message> messageStore = new ObjectStore<>("messages");

	private static ActionListener openListener;

	private static Thread quitHook = new Thread("JustDoIt_quitHook") {
		public void run() {
			Log.action("quitting");
			
			if (chat != null && chat.isShowing())
				Main.addMessage("::close",true,true);

			addMessage("::quit",false,true);
			
			//save everything
			messageStore.saveVector(messages);
		    Reminder.save();

		    //mark that everything has quit properly
			SingleInstanceFile.set("running", "false");

			//save it too
			SingleInstanceFile.save();
		}
	};

	/**
	 * the running method for the application
	 * @param args
	 *   command line arguments:
     *     f or fullog for full logging,
     *     i or info for info logging,
     *     d or debug for debug logging,
     *     none for default logging
	 */
	public static void main(String[] args) {
		if (hasOption(args, "f", "fulllog")) {
    		Log.setLevel(Log.ALL); // for full logging
    		Log.debug("full logging enabled");
		} else if (hasOption(args, "i", "info")) {
    		Log.setLevel(Log.INFO);
    		Log.debug("info logging enabled");
    	} else if (hasOption(args, "d", "debug")) {
    		Log.setLevel(Log.DEBUG);
    		Log.debug("debug logging enabled");
    	} else
    		Log.setLevel(Log.ACTION);
		if (hasOption(args, "C", "no-colorize"))
			Log.colorizeOutput = false;
		if (hasOption(args, "t", "test"))
			TestRunner.main(args);
		
		if (!saveFolder.exists())
			saveFolder.mkdir();
		
		//load the property storage
		SingleInstanceFile.load();
		
		//check if already running
		notifyRunninig();

		//let's go
		startup();
	}
	
	/**
	 * tries to notify already running instances
	 * and closes if another instance responds
	 */
	private static void notifyRunninig() {
		if (SingleInstanceFile.get("running","false").equals("false"))
			return;
		SingleInstanceFile.set("try_open", "true");
		SingleInstanceFile.save();
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			Log.warning("sleeping thread was interrupted", e);
		}
		SingleInstanceFile.load();
		if (!SingleInstanceFile.get("open_ack", "false").equals("true")) {
			ProgressMonitor pm = new ProgressMonitor(null,"checking for running chatbots","waiting for other processes to respond",0,100);
			for (int i = 1; i<100; i++) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					Log.warning("sleeping thread was interrupted", e);
				}
				SingleInstanceFile.load();
				if (SingleInstanceFile.get("open_ack", "false").equals("true"))
					break;
				pm.setProgress(i);
			}
			pm.close();
		}
		if (SingleInstanceFile.get("open_ack", "false").equals("true")) {
			SingleInstanceFile.set("open_ack", "false");
			SingleInstanceFile.set("try_open", "false");
			SingleInstanceFile.save();
			Log.message("opened chat window of open instance");
			System.exit(0);
		}
		//tell the user it hasn't closed
		int n = JOptionPane.showConfirmDialog(
				chat, "The application is stil running or hasn't been closed correctly!\nThis may cause / have caused data loss, run anyway?",
                "running == true",
                JOptionPane.YES_NO_OPTION);
		if (n == JOptionPane.YES_OPTION) {
        	//probably hasn't closed properly
			SingleInstanceFile.set("try_open", "false");
			SingleInstanceFile.save();
            return;
		} else {
			System.exit(0);
		}
	}

	/**
	 * the method for actually running the full application
	 */
	private static void startup() {
		// set the log file
		try {
			Log.setLogFile(new File("log.txt"));
		} catch (FileNotFoundException e) {
			Log.error("Error while trying to set logfile",e);
		}
		//make clear that it shouldn't be started another time
		SingleInstanceFile.set("running", "true");
		//write it to the file
		SingleInstanceFile.save();
		//startup heart beat thread
		responseThread = new Thread(Main::checkStorage,"JustDoIt_responseDaemon");
		responseThread.setDaemon(true);
		responseThread.start();
		//load messages from the file
		messages = messageStore.loadVector(Message.class);
		if (messages.isEmpty()) {
			messages.add(new Message("::init", false));
		}
		//load the reminders
		Reminder.load();
		//build the tray icon
		makeTrayIcon();
		//start up the message processor
		MessageProcessor.init();
		//add the quit hook
		Runtime.getRuntime().addShutdownHook(quitHook);
		//send startup message
		Main.addMessage("::startup",false,true);
		//create a chat window
		chat = new Chat();
	}

	private static void checkStorage() {
		while (true) {
			SingleInstanceFile.load();
			if (SingleInstanceFile.get("try_open", "false").equals("true")) {
				SingleInstanceFile.set("open_ack", "true");
				SingleInstanceFile.set("try_open", "false");
				SingleInstanceFile.save();
				if (openListener!= null)
					openListener.actionPerformed(new ActionEvent(responseThread, ActionEvent.ACTION_PERFORMED, null));
				Log.message("Chat window opened by another instance");
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Log.warning("somebody woke the daemon", e);
			}
		}
	}

	/**
	 * create the tray icon
	 */
	private static void makeTrayIcon() {
		//Check the SystemTray support
        if (!SystemTray.isSupported()) {
            Log.error("SystemTray is not supported!");
            //FIXME: run without SystemTray (chat window only)
            System.exit(1);
        }
        
        //build the pop up menu
        final PopupMenu popup = new PopupMenu();
        //build the tray icon
        //TODO: add an icon
        trayIcon = new TrayIcon((new ImageIcon("")).getImage());
        //get the system tray
        tray = SystemTray.getSystemTray();
        
        // Create a pop up menu components
        MenuItem openItem = new MenuItem("Open");
        MenuItem exitItem = new MenuItem("Exit");
        
        //Add components to pop up menu
        popup.add(openItem);
        popup.add(exitItem);
        
        //set the pop-up menu of the tray to the created menu
        trayIcon.setPopupMenu(popup);
        
        try {
        	//add the icon to the tray
			tray.add(trayIcon);
		} catch (AWTException e) {
			//couldn't add
			Log.error("Error while trying to add the tray icon",e);
			//FIXME: run without SystemTray (chat window only)
			System.exit(1);
		}
        
        openListener = new ActionListener() {
        	public synchronized void actionPerformed(ActionEvent e) {
            	//check if the chat is showing
            	if (chat == null || !chat.isShowing()) {
            		//open a new chat
            		chat = new Chat();
            	} else {
            		//move to front obviously
            		chat.toFront();
            	}
        	}
        };
        
        //open when the tray icon is double clicked
        trayIcon.addActionListener(openListener);
        
        //open when open is clicked
        openItem.addActionListener(openListener);
        
        //exit properly when exit is clicked
        exitItem.addActionListener(e -> {System.exit(0);});
	}

	public static void exitWithoutSave() {
		//remove tray icon
		tray.remove(trayIcon);
		
		Runtime.getRuntime().removeShutdownHook(quitHook);
		
	    //last message
		Log.action("exit");
		System.exit(0);
	}

	/**
	 * send a message
	 * @param text
	 *   the raw text of the message to be added
	 * @param own
	 *   whether the message is one that the user wrote
	 */
	public static void addMessage(String text,boolean own,boolean quiet) {
		//yay, received some
		Log.verbose("new message");
		//create the message object
		Message msg = new Message(text,own);
		//add the message to the list
		messages.add(msg);
		if (chat != null && chat.isShowing()) {
			//add the message to the chat window
			chat.addMessage(msg);
		} else if (!quiet) {
			//notify the user
			trayIcon.displayMessage("message received", text, TrayIcon.MessageType.NONE);
			//TODO: change reminder icon
		}
		//spit it out in the log
		Log.verbose(text);
	}
	
	public static boolean hasOption(String[] args, String shortOpts, String longOpts)
    {
    	if( args == null )	
    	{
    		Log.verbose("no arguments given");
    		//probably you'll never see this message :P
    	}	
    	else
    	{
    		final char seperator = '-';
    		for( int _i = 0; _i < args.length; _i++ )
    		{
    			 if (args[_i].charAt(0)==seperator) {
    				 if (args[_i].charAt(1)==seperator) {
    					 if ( args[_i].substring( 2, args[_i].length() ).equals(longOpts) ) {
    						 return true;
    					 }
    				 } else {
    					 if ( args[_i].substring( 1, args[_i].length() ).equals(shortOpts) ) {
    						 return true;
    					 }
    				 }
    			 }
    		}
    	}
    	return false;
    }

}
