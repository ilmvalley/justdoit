package justDoIt;

import java.io.Serializable;

/**
 * a Serializble form of a chat message
 */
public class Message implements Serializable {

	private static final long serialVersionUID = -711571779132624479L;
	
	/**
	 * the contents of the message
	 */
	public final String text;
	
	/**
	 * whether the message is from the user
	 */
	public final boolean own;

	public Message(String text, boolean own) {
		this.text = text;
		this.own = own;
	}

}
