package justDoIt;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.Enumeration;
import java.util.function.Consumer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

public class ResourceLoader {
	
	public JarFile jarFile = null;
	public static final ResourceLoader global = new ResourceLoader();

	public class ResourceNotFoundException extends FileNotFoundException {
		private static final long serialVersionUID = -3547413623636304260L;

		/**
		 * constructs a resource not found exception
		 * @param path
		 *   the resource that was not found
		 */
		public ResourceNotFoundException(String path) {
			super('"' + path + "\" resource couldn't be found");
		}
	}
	
	private ResourceLoader() {
		URL resourceURL = ResourceLoader.class.getResource("test.txt");
		if (resourceURL == null) {
			Log.error("ResourceLoader couldn't initialize: \"test.txt\" wasn't found");
			return;
		}
		String resourceLocation = resourceURL.getFile().replaceFirst("file:", "");
		if (resourceLocation.contains(".jar!")) {
			try {
				jarFile = new JarFile(resourceLocation.substring(0, resourceLocation.indexOf("!")));
			} catch (IOException e) {
				Log.error("jar file couldn't be opened", e);
			}
		}
	}
	
	private String packageToPath(String path, Class<?> caller) {
		if (path.startsWith(File.separator) || caller == null)
			return path;
		else
			return caller.getPackageName().replaceAll("\\.", File.separator) + File.separator + path;
	}
	
	public boolean isDirectory(String path, Class<?> fromClass) throws ResourceNotFoundException {
		if (jarFile==null) {
			URL resourceURL;
			if (fromClass == null)
				resourceURL = ResourceLoader.class.getResource((path.startsWith("/") ? "" : "/") + path);
			else
				resourceURL = fromClass.getResource(path);
			if (resourceURL == null)
				throw new ResourceNotFoundException(path);
			return new File(resourceURL.getFile()).isDirectory();
		} else {
			String jarPath;
			jarPath = packageToPath(path,fromClass);
			ZipEntry entry = jarFile.getEntry(jarPath);
			if (entry == null)
				throw new ResourceNotFoundException(path);
			return entry.isDirectory();
		}
	}
	
	public Reader getReader(String path, Class<?> fromClass) throws IOException {
		if (jarFile==null || !isDirectory(path,fromClass)) {
			InputStream inputStream = fromClass.getResourceAsStream(path);
			return new InputStreamReader(inputStream);
		} else {
			String jarPath = packageToPath(path,fromClass);
			Log.debug("reading folder "+jarPath);
			Enumeration<JarEntry> en = jarFile.entries();
			String dirList = "";
			int needSeperator = jarPath.endsWith("/") ? 0 : 1;
			boolean first = true;
			while (en.hasMoreElements()) {
				String el = en.nextElement().getName();
				if (!el.startsWith(jarPath))
					continue;
				String childElement = el.substring(jarPath.length() + needSeperator);
				if (childElement.isEmpty())
					continue;
				Log.info("folder contains element " + childElement);
				if (childElement.contains("/")) {
					if (childElement.indexOf("/") == childElement.length()) {
						dirList += (first ? "" : "\n") + childElement;
						first = false;
					}
				} else {
					dirList += (first ? "" : "\n") + childElement;
					first = false;
				}
			}
			Log.verbose("folder content list:\n" + dirList);
			return new StringReader(dirList);
		}
	}

	public void forEachInFolder(String folder, Consumer<String> consumer, Class<?> fromClass) {
		String jarPath = packageToPath(folder,fromClass);
		Log.debug("reading folder "+jarPath);
		if (jarFile == null) {
			URL resourceURL;
			if (fromClass == null)
				resourceURL = ResourceLoader.class.getResource((folder.startsWith("/") ? "" : "/") + folder);
			else
				resourceURL = fromClass.getResource(folder);
			if (resourceURL == null)
				return;
			for (File content : new File(resourceURL.getFile()).listFiles())
				consumer.accept(content.getName());
		} else {
			Enumeration<JarEntry> en = jarFile.entries();
			String fullJarPath = jarPath;
			if (!jarPath.endsWith("/"))
				fullJarPath += "/";
			while (en.hasMoreElements()) {
				String el = en.nextElement().getName();
				if (!el.startsWith(jarPath))
					continue;
				if (el.equals(fullJarPath))
					continue;
				String childName = el;
				if (childName.endsWith("/"))
					childName = el.substring(0, el.length() - 1);
				if (childName.lastIndexOf("/") != fullJarPath.length()-1)
					continue;
				Log.info("folder contains element " + el);
				consumer.accept(el);
			}
		}
	}
}
